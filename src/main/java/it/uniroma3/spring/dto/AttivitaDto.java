package it.uniroma3.spring.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import it.uniroma3.spring.model.CentroFormazione;

public class AttivitaDto {

	@NotNull
	@NotEmpty
	private String nome;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date data;

	@NotNull
	@DateTimeFormat(pattern = "HH:mm")
	private Date ora;
	
	//@NotNull
	private Long centroFormazione;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getOra() {
		return ora;
	}

	public void setOra(Date ora) {
		this.ora = ora;
	}

	public Long getCentroFormazione() {
		return centroFormazione;
	}

	public void setCentroFormazione(Long centroFormazione) {
		this.centroFormazione = centroFormazione;
	}
	
	
}
