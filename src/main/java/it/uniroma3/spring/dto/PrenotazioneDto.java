package it.uniroma3.spring.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PrenotazioneDto {

	@NotNull
	@NotEmpty
	@Email
	private String allievo;
	
	@NotNull
	Long attivita;

	public String getAllievo() {
		return allievo;
	}

	public void setAllievo(String allievo) {
		this.allievo = allievo;
	}

	public Long getAttivita() {
		return attivita;
	}

	public void setAttivita(Long attivita) {
		this.attivita = attivita;
	}

}
