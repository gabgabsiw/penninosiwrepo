package it.uniroma3.spring.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ResponsabileDto {
	@NotNull
	@NotEmpty
	@Email
	private String responsabile;
	
	@NotNull
	Long centro;

	public String getResponsabile() {
		return responsabile;
	}

	public void setResponsabile(String responsabile) {
		this.responsabile = responsabile;
	}

	public Long getCentro() {
		return centro;
	}

	public void setCentro(Long centro) {
		this.centro = centro;
	}
	
	
}
