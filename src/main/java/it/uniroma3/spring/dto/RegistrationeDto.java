package it.uniroma3.spring.dto;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public class RegistrationeDto {

	@NotNull
	@NotEmpty
	private String nome;

	@NotNull
	@NotEmpty
	private String cognome;

	@NotNull
	@NotEmpty
	private String telefono;

	@NotNull
	@NotEmpty
	private String password;
	
	private String ruolo;	
	
	@NotNull
	@NotEmpty
	@Email
	private String email;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")//MM-dd-yyyy")
	private Date dataNascita;
	
	@NotNull
	@NotEmpty	
	private String luogoNascita;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getLuogoNascita() {
		return luogoNascita;
	}

	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}


}
