package it.uniroma3.spring.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "UTENTI")
public class Utente {
	@Id
	@Column(nullable=false)
	private String email;

	@Column(nullable=false)
	private String nome;

	@Column(nullable=false)
	private String cognome;	
	
	@Column(nullable = false)
	private String password;

	@Column( nullable = false)
	private String ruolo;
	
	@Column(nullable=false)
	private String telefono;
	
	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")//MM-dd-yyyy")
	@Temporal(TemporalType.DATE)
	private Date dataNascita;
	
	@Column(nullable=false)
	private String luogoNascita;
	
	@ManyToMany
	private List<Attivita> attivita;
	
	public Utente() {}
	
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getLuogoNascita() {
		return luogoNascita;
	}

	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	public List<Attivita> getAttivita() {
		return attivita;
	}

	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}

}