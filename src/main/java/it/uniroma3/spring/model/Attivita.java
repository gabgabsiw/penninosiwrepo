package it.uniroma3.spring.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "ATTIVITA")
public class Attivita {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable=false)
	private String nome;

	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date data;

	@Column(nullable = false)
	@DateTimeFormat(pattern = "HH:mm:ss")
	@Temporal(TemporalType.TIME)
	private Date ora;

	@ManyToOne
	private CentroFormazione centroFormazione;

	@ManyToMany(mappedBy="attivita", fetch = FetchType.EAGER)
	private List<Utente> allievi;


	public Attivita() {}

	public Attivita(String nome, Date data, Date ora, CentroFormazione centroFormazione) {
		super();
		this.nome = nome;
		this.data = data;
		this.ora = ora;
		this.centroFormazione = centroFormazione;
		this.allievi = new ArrayList<Utente>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getOra() {
		return ora;
	}

	public void setOra(Date ora) {
		this.ora = ora;
	}

	public CentroFormazione getCentroFormazione() {
		return centroFormazione;
	}

	public void setCentroFormazione(CentroFormazione centroFormazione) {
		this.centroFormazione = centroFormazione;
	}

	public List<Utente> getAllievi() {
		return allievi;
	}

	public void setAllievi(List<Utente> allievi) {
		this.allievi = allievi;
	}

}