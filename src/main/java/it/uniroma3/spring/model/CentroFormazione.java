package it.uniroma3.spring.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "CENTRI_FORMAZIONE")
public class CentroFormazione {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable=false)
	private String nome;

	@Column(nullable=false)
	private String indirizzo;
	
	@Column(nullable=false)
	private String email;
	
	@Column(nullable = false)
	private String telefono;
	
	@Column(nullable = false, name="capienza")
	private int capienzaMax;
	
	@OneToMany(mappedBy="centroFormazione", fetch = FetchType.EAGER)
	private List<Attivita> attivita;
	
	@OneToOne
	private Utente responsabile;
	
	public CentroFormazione() {}


	public CentroFormazione(Long id, String nome, String indirizzo, String email, String telefono, int capienzaMax,
			List<Attivita> attivita, Utente responsabile) {
		super();
		this.id = id;
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.email = email;
		this.telefono = telefono;
		this.capienzaMax = capienzaMax;
		this.attivita = attivita;
		this.responsabile = responsabile;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getIndirizzo() {
		return indirizzo;
	}


	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public int getCapienzaMax() {
		return capienzaMax;
	}


	public void setCapienzaMax(int capienzaMax) {
		this.capienzaMax = capienzaMax;
	}


	public List<Attivita> getAttivita() {
		return attivita;
	}


	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}


	public Utente getResponsabile() {
		return responsabile;
	}


	public void setResponsabile(Utente responsabile) {
		this.responsabile = responsabile;
	}
	
}
