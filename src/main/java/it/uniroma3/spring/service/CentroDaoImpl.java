package it.uniroma3.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.spring.dao.CentroDao;
import it.uniroma3.spring.dao.UserDetailsDao;
import it.uniroma3.spring.dto.CentroDto;
import it.uniroma3.spring.model.Attivita;
import it.uniroma3.spring.model.CentroFormazione;
import it.uniroma3.spring.model.Utente;

@Service
public class CentroDaoImpl implements CentroDao{

	@Autowired
	private SessionFactory sessionFactory;	

	@Autowired
	private UserDetailsDao userService;	
	
	@Override
	@Transactional(readOnly = true)
	public CentroFormazione findById(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (CentroFormazione) session.get(CentroFormazione.class, id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CentroFormazione> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<CentroFormazione> centriList = session.createQuery("FROM it.uniroma3.spring.model.CentroFormazione").list();
		return centriList;
	}

	@Override
	@Transactional
	public void addAttivita(CentroFormazione centro, Attivita attivita) {
		this.findById(centro.getId()).getAttivita().add(attivita);
	}

	@Override
	@Transactional
	public String addCentro(CentroDto dto) {
		if(this.findByNomeAndIndirizzo(dto.getNome(), dto.getIndirizzo()).isEmpty()) {
			Utente resp = this.userService.findById(dto.getResponsabile());
			if(resp==null) return "Il responsabile inserito non esiste";			
			CentroFormazione centro = new CentroFormazione();
			centro.setNome(dto.getNome());
			centro.setCapienzaMax(dto.getCapienzaMax());
			centro.setEmail(dto.getEmail());
			centro.setIndirizzo(dto.getIndirizzo());
			centro.setTelefono(dto.getTelefono());
			centro.setAttivita(new ArrayList<Attivita>());
			centro.setResponsabile(resp);
			
			Session session = this.sessionFactory.getCurrentSession();
			session.persist(centro);
			return null;
		} else { return "Il centro esiste già"; }
	}

	@Override
	@Transactional(readOnly = true)
	public List<CentroFormazione> findByNomeAndIndirizzo(String nome, String indirizzo) {
		Session session = this.sessionFactory.getCurrentSession();
		String q = "FROM it.uniroma3.spring.model.CentroFormazione C WHERE C.nome='"+nome+"' AND C.indirizzo='"+indirizzo+"'";
		List<CentroFormazione> centriList = session.createQuery(q).list();
		return centriList;		
	}

	@Override
	@Transactional(readOnly = true)
	public boolean isAutorizzato(Long idCentro, String email) {
		CentroFormazione c = this.findById(idCentro);
		Utente u = c.getResponsabile();
		return u.getEmail().equals(email) || u.getRuolo().equals("ROLE_IMPIEGATO");		
	}
}
