package it.uniroma3.spring.service;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.spring.dao.UserDetailsDao;
import it.uniroma3.spring.dto.RegistrationeDto;
import it.uniroma3.spring.model.Attivita;
import it.uniroma3.spring.model.Utente;

@Service("userDetailsService")
public class UserDetailsDaoImp implements UserDetailsDao {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Utente findUserByEmail(String email) {
		return sessionFactory.getCurrentSession().get(Utente.class, email);
	}

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Utente utente = this.findUserByEmail(username);
		UserBuilder builder = null;
		if (utente != null) {
			System.out.println("User Found");
			builder = org.springframework.security.core.userdetails.User.builder();
			builder.username(utente.getEmail());
			builder.password(utente.getPassword());
			builder.authorities(utente.getRuolo());
		} else {
			System.out.println("UserNotFound");
			throw new UsernameNotFoundException("User not found.");
		}
		return builder.build();
	}

	@Override
	@Transactional
	public boolean addUser(RegistrationeDto dto) {
		if(this.findUserByEmail(dto.getEmail())==null) {
			Utente utente = new Utente();
			utente.setAttivita(new ArrayList<Attivita>());
			utente.setCognome(dto.getCognome());
			utente.setDataNascita(dto.getDataNascita());
			utente.setEmail(dto.getEmail());
			utente.setLuogoNascita(dto.getLuogoNascita());
			utente.setNome(dto.getNome());
			utente.setPassword(this.passwordEncoder.encode(dto.getPassword()));
			String ruolo = dto.getRuolo();
			if(ruolo==null) { ruolo = "ROLE_ALLIEVO"; }
			utente.setRuolo(ruolo);
			utente.setTelefono(dto.getTelefono());
			
			Session session = this.sessionFactory.getCurrentSession();
			session.persist(utente);
			return true;
		} else { return false; }
	}
	
	@Override
	@Transactional(readOnly = true)
	public Utente findById(String id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Utente) session.get(Utente.class, id);
	}
}
