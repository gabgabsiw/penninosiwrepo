package it.uniroma3.spring.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.spring.dao.AttivitaDao;
import it.uniroma3.spring.dao.CentroDao;
import it.uniroma3.spring.dao.UserDetailsDao;
import it.uniroma3.spring.dto.AttivitaDto;
import it.uniroma3.spring.model.Attivita;
import it.uniroma3.spring.model.CentroFormazione;
import it.uniroma3.spring.model.Utente;

@Service
public class AttivitaDaoImpl implements AttivitaDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private UserDetailsDao userService;
	
	@Autowired
	private CentroDao centroDao;

	@Override
	@Transactional
	public void persist(Attivita attivita) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(attivita);
	}
	
	@Override
	@Transactional
	public Attivita addAttivita(AttivitaDto dto) {
		CentroFormazione cf = this.centroDao.findById(dto.getCentroFormazione());
		if(this.findByNomeAndCentro(dto.getNome(), cf).isEmpty()) {
			Attivita attivita = new Attivita(dto.getNome(), dto.getData(), dto.getOra(), cf);
			Session session = this.sessionFactory.getCurrentSession();
			
			this.centroDao.addAttivita(cf, attivita);
			
			session.persist(attivita);
			return attivita;
		} else { return null; }
	}
	
	@Override
	@Transactional
	public void addAllievo(Long idAttivita, String idAllievo) {
		Attivita att = this.findById(idAttivita);
		Utente all = this.userService.findById(idAllievo);
		if(att==null || all==null) return;
		att.getAllievi().add(all);
		all.getAttivita().add(att);
	}	
	
	@Override
	@Transactional(readOnly = true)
	public List<Attivita> findByNomeAndCentro(String nome, CentroFormazione centro) {
		Session session = this.sessionFactory.getCurrentSession();
		String q = "FROM it.uniroma3.spring.model.Attivita A WHERE A.nome='"+nome+"' AND A.centroFormazione='"+centro.getId()+"'";
		List<Attivita> attivitaList = session.createQuery(q).list();
		return attivitaList;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Attivita> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Attivita> attivitaList = session.createQuery("FROM it.uniroma3.spring.model.Attivita").list();
		return attivitaList;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Attivita> findByCentro(Long idCentro) {
		Session session = this.sessionFactory.getCurrentSession();
		String q = "FROM it.uniroma3.spring.model.Attivita A WHERE A.centroFormazione="+idCentro;
		List<Attivita> attivitaList = session.createQuery(q).list();
		return attivitaList;
	}

	@Override
	@Transactional(readOnly = true)
	public Attivita findById(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Attivita) session.get(Attivita.class, id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public boolean isAutorizzato(Long idAttivita, String email) {
		CentroFormazione c = this.findById(idAttivita).getCentroFormazione();
		return this.centroDao.isAutorizzato(c.getId(), email);		
	}

}
