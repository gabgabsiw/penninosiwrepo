package it.uniroma3.spring.dao;

import org.springframework.security.core.userdetails.UserDetailsService;

import it.uniroma3.spring.dto.RegistrationeDto;
import it.uniroma3.spring.model.Utente;

public interface UserDetailsDao extends UserDetailsService{
	Utente findUserByEmail(String email);
	
	boolean addUser(RegistrationeDto dto);
	
	public Utente findById(String id);
}
