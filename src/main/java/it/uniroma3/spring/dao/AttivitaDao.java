package it.uniroma3.spring.dao;

import java.util.List;

import it.uniroma3.spring.dto.AttivitaDto;
import it.uniroma3.spring.model.Attivita;
import it.uniroma3.spring.model.CentroFormazione;

public interface AttivitaDao {
	
	void persist(Attivita attivita);
	
	Attivita findById(Long id);
	
	List<Attivita> findAll();
	
	List<Attivita> findByCentro(Long idCentro);

	Attivita addAttivita(AttivitaDto dto);
	
	List<Attivita> findByNomeAndCentro(String nome, CentroFormazione centro);
	
	void addAllievo(Long idAttivita, String idAllievo);
	
	boolean isAutorizzato(Long idAttivita, String email);
}
