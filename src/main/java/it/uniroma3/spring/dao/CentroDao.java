package it.uniroma3.spring.dao;

import java.util.List;

import it.uniroma3.spring.dto.CentroDto;
import it.uniroma3.spring.model.Attivita;
import it.uniroma3.spring.model.CentroFormazione;

public interface CentroDao {
	
	CentroFormazione findById(Long id);
	
	List<CentroFormazione> findAll();
	
	void addAttivita(CentroFormazione centro, Attivita attivita);

	String addCentro(CentroDto dto);
	
	List<CentroFormazione> findByNomeAndIndirizzo(String nome, String indirizzo);
	
	boolean isAutorizzato(Long idCentro, String email);
	
}
