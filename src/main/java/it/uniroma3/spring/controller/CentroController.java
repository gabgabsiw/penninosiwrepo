package it.uniroma3.spring.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import it.uniroma3.spring.dao.AttivitaDao;
import it.uniroma3.spring.dao.CentroDao;
import it.uniroma3.spring.dao.UserDetailsDao;
import it.uniroma3.spring.dto.CentroDto;
import it.uniroma3.spring.dto.ResponsabileDto;
import it.uniroma3.spring.model.Utente;

@Controller
public class CentroController {
	
	@Autowired
	private CentroDao centroDao;
	
	@Autowired
	private AttivitaDao attivitaDao;
	
	@Autowired
	private UserDetailsDao userService;

	@GetMapping("/centro-{id}")
	public String centro(Model model, @PathVariable("id") Long id, Authentication authentication) {
		boolean isResp = authentication!=null && this.centroDao.isAutorizzato(id, authentication.getName());
		ResponsabileDto r = new ResponsabileDto();
		r.setCentro(id);
		model.addAttribute("self", this.centroDao.findById(id));
		model.addAttribute("responsabile", r);
		model.addAttribute("isResp", isResp);
		return "centro";
	} 	
	
	@GetMapping("/centri")
	public String centri(Model model) {
		model.addAttribute("listaCentri", this.centroDao.findAll());
		return "centri";
	}
	
	@PostMapping(value = "/assegnaResponsabile")
	public String assegnaResponsabile(@ModelAttribute("responsabile") @Valid ResponsabileDto responsabileDto, 
			BindingResult result, WebRequest request, Errors errors, Model model) {
		Long id = responsabileDto.getCentro();
		if (!result.hasErrors()) {
			Utente resp = this.userService.findById(responsabileDto.getResponsabile());
			this.centroDao.findById(id).setResponsabile(resp);
		}
		return "redirect:/centro-"+id;
	}
	
	@GetMapping(value = "/nuovoCentro")
	public String nuovoCentro(WebRequest request, Model model) {
		CentroDto c = new CentroDto();
		model.addAttribute("centro", c);
		return "formAggiungiCentro";
	}	
	
	@PostMapping(value = "/inserisciCentro")
	public ModelAndView inserisciCentro(@ModelAttribute("centro") @Valid CentroDto centroDto, 
			BindingResult result, WebRequest request, Errors errors, Model model) {

		if (!result.hasErrors()) {
			String add = this.centroDao.addCentro(centroDto);
			if(add==null) {
				return new ModelAndView("centri", "listaCentri", this.centroDao.findAll());
			} else {
				return new ModelAndView("formAggiungiCentro", "exists", add);
			}
			
		} else {
			return new ModelAndView("formAggiungiCentro", "centro", centroDto);
		}
	}		
}
