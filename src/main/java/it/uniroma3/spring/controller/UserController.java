package it.uniroma3.spring.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import it.uniroma3.spring.dao.UserDetailsDao;
import it.uniroma3.spring.dto.RegistrationeDto;

@Controller
public class UserController {

	@Autowired
	private UserDetailsDao userService;
	
	@GetMapping(value = "/registra")
	public String registra(WebRequest request, Model model) {
		RegistrationeDto userDto = new RegistrationeDto();
		model.addAttribute("user", userDto);
		return "formAggiungiUtente";
	}

	@PostMapping(value = "/inserisciUtente")
	public ModelAndView inserisciUtente(@ModelAttribute("user") @Valid RegistrationeDto accountDto, 
			BindingResult result, WebRequest request, Errors errors, Model model) {

		if (!result.hasErrors()) {
			if(this.userService.addUser(accountDto)) {
				return new ModelAndView("index");
			} else {
				return new ModelAndView("formAggiungiUtente", "exists", "Questa email è stata già utilizzata");
			}
			
		} else {
			return new ModelAndView("formAggiungiUtente", "user", accountDto);
		}
	}
	
}
