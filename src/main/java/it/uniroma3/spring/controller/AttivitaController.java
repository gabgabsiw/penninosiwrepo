package it.uniroma3.spring.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;

import it.uniroma3.spring.dao.AttivitaDao;
import it.uniroma3.spring.dto.AttivitaDto;
import it.uniroma3.spring.dto.PrenotazioneDto;
import it.uniroma3.spring.model.Attivita;
import it.uniroma3.spring.model.Utente;

@Controller
public class AttivitaController {

	@Autowired
	private AttivitaDao attivitaDao;
	
	@GetMapping(value = "/centro-{id}/nuovaAttivita")
	public String nuovaAttivita(WebRequest request, Model model, @PathVariable("id") Long id) {
		AttivitaDto a = new AttivitaDto();
		a.setCentroFormazione(id);
		model.addAttribute("attivita", a);
		return "formAggiungiAttivita";
	}	

	@GetMapping("/attivita-{id}")
	public String attivita(Model model, @PathVariable("id") Long id, Authentication authentication) {
		boolean isResp = authentication!=null && this.attivitaDao.isAutorizzato(id, authentication.getName());
		Attivita a = this.attivitaDao.findById(id);
		List<Utente> allievi = a.getAllievi();
		boolean tuttoPrenotato = allievi.size()==a.getCentroFormazione().getCapienzaMax();
		PrenotazioneDto p = new PrenotazioneDto();
		p.setAttivita(id);
		model.addAttribute("self", a);
		model.addAttribute("tuttoPrenotato", tuttoPrenotato);
		model.addAttribute("prenotazione", p);
		model.addAttribute("isResp", isResp);
		return "attivitasingola";
	} 	

	@PostMapping(value = "/inserisciPrenotato")
	public String inserisciPrenotato(@ModelAttribute("prenotazione") @Valid PrenotazioneDto prenotazioneDto, 
			BindingResult result, WebRequest request, Errors errors, Model model) {
		Long id = prenotazioneDto.getAttivita();
		if (!result.hasErrors()) {
			this.attivitaDao.addAllievo(id, prenotazioneDto.getAllievo());
		}
		return "redirect:/attivita-"+id;
	}
	
	@PostMapping(value = "/inserisciAttivita")
	public String inserisciAttivita(@ModelAttribute("attivita") @Valid AttivitaDto attivitaDto, 
			BindingResult result, WebRequest request, Errors errors, Model model) {
		if (!result.hasErrors()) {
			Attivita na = this.attivitaDao.addAttivita(attivitaDto);
			if(na!=null) {
				return "redirect:/attivita-"+na.getId();
			} else {
				model.addAttribute("exists", "Attività già esistente");
			}
		}
		model.addAttribute("attivita", attivitaDto);
		return "formAggiungiAttivita";
	}	
}
